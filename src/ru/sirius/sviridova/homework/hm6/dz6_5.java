package ru.sirius.sviridova.homework.hm6;

public class dz6_5 {
    public static void main(String[] args){
        Salary sal = new Salary(50000, 30000);
        System.out.format("%d %n %s %n ", sal.getSalary(), sal.toString());
        sal.setSalary(35000);
        System.out.format("%s %n ", sal.toString());
    }
    
}
