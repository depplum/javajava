package ru.sirius.sviridova.homework.hm2;

public class dz2_2 {
    public static void main(String[] args) {
        int arr[] = new int[]{2,5,43,56,77,23,11,14};
        int shift = 2;
        // сделать без дополнительного массива
        int[] new_arr = new int[arr.length];
        for (int z = 0; z < shift; z++){
            new_arr[z] = 0;
        }
        for (int i = 0; i < new_arr.length - shift; i++){
            new_arr[i + shift] = arr[i];
        }
        for(int j=0; j < new_arr.length; j++){
            System.out.format("%d ", new_arr[j]);
        }
    }
}       
