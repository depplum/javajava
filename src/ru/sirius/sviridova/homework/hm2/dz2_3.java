package ru.sirius.sviridova.homework.hm2;

public class dz2_3 {
    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5,6,7,8,9,10};
        // переделать
        for (int i = 0; i < arr.length; i +=2){
            int temp = arr[i];
            arr[i] = arr[i + 1];
            arr[i + 1] = temp;
        }

        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }

    }
} 