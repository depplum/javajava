package ru.sirius.sviridova.homework.hm8;

public class Student {
    private String name;
    private String group;
    public static int count;

    public Student(String name, String group){
        this.name = name;
        this.group = group;
        count++;

    }
    public String getName(){
        return name;
    }
    public String getGroup(){
        return group;
    }
    public void setGroup(String group){
        this.group = group;
    }

    
}
